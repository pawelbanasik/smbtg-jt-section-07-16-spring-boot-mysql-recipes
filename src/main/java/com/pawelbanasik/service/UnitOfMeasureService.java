package com.pawelbanasik.service;

import java.util.Set;

import com.pawelbanasik.command.UnitOfMeasureCommand;

public interface UnitOfMeasureService {

	Set<UnitOfMeasureCommand> listAllUoms();
}
