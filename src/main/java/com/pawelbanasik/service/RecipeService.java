package com.pawelbanasik.service;

import java.util.Set;

import com.pawelbanasik.command.RecipeCommand;
import com.pawelbanasik.domain.Recipe;

public interface RecipeService {

	Set<Recipe> getRecipes();

	Recipe findById(Long l);

	RecipeCommand findCommandById(Long l);

	RecipeCommand saveRecipeCommand(RecipeCommand command);

	void deleteById(Long idToDelete);
}
