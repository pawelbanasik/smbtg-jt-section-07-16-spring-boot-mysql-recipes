package com.pawelbanasik.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import com.pawelbanasik.command.NotesCommand;
import com.pawelbanasik.domain.Notes;

@Component
public class NotesToNotesCommand implements Converter<Notes, NotesCommand> {

	// @Synchronized
	@Nullable
	@Override
	public synchronized NotesCommand convert(Notes source) {
		if (source == null) {
			return null;
		}

		final NotesCommand notesCommand = new NotesCommand();
		notesCommand.setId(source.getId());
		notesCommand.setRecipeNotes(source.getRecipeNotes());
		return notesCommand;
	}
}
