package com.pawelbanasik.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import com.pawelbanasik.command.NotesCommand;
import com.pawelbanasik.domain.Notes;

@Component
public class NotesCommandToNotes implements Converter<NotesCommand, Notes> {

	// @Synchronized
	@Nullable
	@Override
	public synchronized Notes convert(NotesCommand source) {
		if (source == null) {
			return null;
		}

		final Notes notes = new Notes();
		notes.setId(source.getId());
		notes.setRecipeNotes(source.getRecipeNotes());
		return notes;
	}
}
