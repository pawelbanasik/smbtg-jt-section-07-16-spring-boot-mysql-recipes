package com.pawelbanasik.repository;

import org.springframework.data.repository.CrudRepository;

import com.pawelbanasik.domain.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {
}
