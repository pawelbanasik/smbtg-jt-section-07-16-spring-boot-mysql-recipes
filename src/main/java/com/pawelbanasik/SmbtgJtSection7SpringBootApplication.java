package com.pawelbanasik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmbtgJtSection7SpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmbtgJtSection7SpringBootApplication.class, args);
	}
}
