package com.pawelbanasik.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

import com.pawelbanasik.controller.IndexController;
import com.pawelbanasik.domain.Recipe;
import com.pawelbanasik.service.RecipeService;

public class IndexControllerTest {

	private IndexController indexController;
	private MockMvc mockMvc;

	@Mock
	private Model model;

	@Mock
	private RecipeService recipeService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		indexController = new IndexController(recipeService);
		mockMvc = MockMvcBuilders.standaloneSetup(indexController).build();

	}

	// spradzany sam get
	@Test
	public void mockMvcTest() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(view().name("index"));
	}

	// fake set of recipes to check if view name is equal
	// number of invocations
	@Test
	public void getIndexPageTest() {
		// given
		Set<Recipe> recipes = new HashSet<>();
		Recipe recipeOne = new Recipe();
		recipeOne.setId(1L);
		Recipe recipeTwo = new Recipe();
		recipeTwo.setId(2L);
		recipes.add(recipeOne);
		recipes.add(recipeTwo);
		when(recipeService.getRecipes()).thenReturn(recipes);
		ArgumentCaptor<Set<Recipe>> argumentCaptor = ArgumentCaptor.forClass(Set.class);

		// when
		String viewName = indexController.getIndexPage(model);

		// then
		assertEquals("index", viewName);
		verify(recipeService, times(1)).getRecipes();
		verify(model, times(1)).addAttribute(eq("recipes"), argumentCaptor.capture());
		Set<Recipe> setInController = argumentCaptor.getValue();
		assertEquals(2, setInController.size());
	}

}
